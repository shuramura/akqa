﻿using System.Reflection;
using System.Web.Http;
using Akqa;
using Akqa.Core;
using Autofac;
using Autofac.Integration.WebApi;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Startup))]

namespace Akqa
{
    public class Startup
    {
        public void Configuration(IAppBuilder appBuilder)
        {
            HttpConfiguration config = new HttpConfiguration();

            WebApiConfig.Register(config);

            appBuilder.UseAutofacMiddleware(BuildContainer());
            appBuilder.UseAutofacWebApi(config);
            appBuilder.UseWebApi(config);
        }

        private static IContainer BuildContainer()
        {
            ContainerBuilder builder = new ContainerBuilder();

            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            builder.RegisterType<NumberToWordConveter>().AsImplementedInterfaces().SingleInstance();

            return builder.Build();
        }
    }
}