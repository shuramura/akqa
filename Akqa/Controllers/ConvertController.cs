﻿using System.Web.Http;
using Akqa.Core;

namespace Akqa.Controllers
{
    public class ConvertController : ApiController
    {
        private readonly INumberToWordConveter _conveter;

        public ConvertController(INumberToWordConveter conveter)
        {
            _conveter = conveter;
        }

        [HttpGet]
        public IHttpActionResult Get(decimal amount)
        {
            if (amount < 0)
            {
                return Json(new {error = "The amount should be greater or equal to zero."});
            }

            return Json(new {value = _conveter.Convert(amount)});
        }
    }
}