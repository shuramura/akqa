﻿namespace Akqa.Core
{
    public interface INumberToWordConveter
    {
        string Convert(decimal number);
    }
}