﻿using Microsoft.Owin.Testing;
using NUnit.Framework;

namespace Akqa.Tests
{
    [SetUpFixture]
    public class Setup
    {
        public static TestServer Server { get; private set; }

        [OneTimeSetUp]
        public void Init()
        {
            Server = TestServer.Create<Startup>();
        }

        [OneTimeTearDown]
        public void Cleanup()
        {
            Server?.Dispose();
        }
    }
}