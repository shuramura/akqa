﻿using System.Net.Http;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Akqa.Tests.Controllers
{
    [TestFixture]
    public class ConvertControllerTests
    {
        [TestCase(123.45, "one hundred twenty three dollars forty five cents", null)]
        [TestCase(-20, null, "The amount should be greater or equal to zero.")]
        public async Task Convert_Test(decimal value, string expectedValue, string expectedError)
        {
            // Act
            HttpResponseMessage response = await Setup.Server.HttpClient.GetAsync("/api/convert?amount=" + value);

            // Assert
            Result result = await response.Content.ReadAsAsync<Result>();

            Assert.That(result, Is.Not.Null);
            Assert.That(result.Value, Is.EqualTo(expectedValue));
            Assert.That(result.Error, Is.EqualTo(expectedError));
        }

        public class Result
        {
            public string Value { get; set; }
            public string Error { get; set; }
        }
    }
}