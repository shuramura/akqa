# AKQA Code Test #

## THE TASK ##
Develop a web application that performs the following functions:

1. Capture a person�s name and a number representing a dollar amount
2. Convert this number into words
3. Render the captured name and converted dollar amount (as words)

### Solution description ###

The solution consists from three parts:

* Web Api server
* Fron end javascript application
* Unit tests

The web api and from end parts are located in Akqa project. 

### How do I get set up? ###
To get it running you have to clone the repository, open it in Visual Studio 2015 and run Akqa project. All dependencies will be downloaded automaticaly from Nuget server.

