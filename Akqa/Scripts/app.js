﻿function App(args) {
    this._args = $.extend({ name: "#name", amount: "#amount", submit: "#submit", output: "#output" }, args);
    this._init();
}

App.prototype = {
    _init: function () {
        this._isLoading = false;
        $(this._args.submit).click($.proxy(this._convert, this));
        $(this._args.name).change($.proxy(this._validate, this));
        $(this._args.amount).change($.proxy(this._validate, this));

        this._validate();
    },

    _convert: function() {
        var name = $(this._args.name).val();
        var amount = $(this._args.amount).val();
        var self = this;
        this._isLoading = true;
        this._validate();
        $.getJSON(this._args.url, { amount: amount },
                function (data) {
                    if (data.error) {
                        alert(data.error);
                    } else {
                        self._addOutput(name, data.value);
                    }
                })
            .done(function () {
                self._isLoading = false;
                self._validate();
            });
    },

    _validate: function() {
        var name = $(this._args.name).val();
        var amount = $(this._args.amount).val();
        if (this._isLoading || name.length === 0 || amount.length === 0) {
            $(this._args.submit).attr("disabled", "disabled");
        } else {
            $(this._args.submit).removeAttr("disabled");
        }
    },

    _addOutput: function(name, amount) {
        var output = $(this._args.output).val();
        if (output.length > 0) {
            output += "\n";
        }
        output += name + ": " + amount;
        $(this._args.output).val(output);
    }
};