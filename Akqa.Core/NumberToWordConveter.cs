﻿using Nut;

namespace Akqa.Core
{
    public class NumberToWordConveter : INumberToWordConveter
    {
        public string Convert(decimal number)
        {
            return number.ToText(Currency.USD);
        }
    }
}