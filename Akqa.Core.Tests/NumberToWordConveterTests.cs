﻿using NUnit.Framework;

namespace Akqa.Core.Tests
{
    [TestFixture]
    public class NumberToWordConveterTests
    {
        [TestCase(123.45, "one hundred twenty three dollars forty five cents")]
        public void Convert_Test(decimal value, string expected)
        {
            // Arrange
            NumberToWordConveter instance = new NumberToWordConveter();

            // Act
            string result = instance.Convert(value);

            // Assert
            Assert.That(result, Is.EqualTo(expected));
        }
    }
}